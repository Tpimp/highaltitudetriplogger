﻿using System;
using Microsoft.SPOT.Hardware;


namespace I2CUtilities
{
    public class I2CBase
    {
        static I2CDevice device;
        I2CDevice.Configuration config;


        public I2CBase(I2CDevice.Configuration Config)
        {
            // Replace with actual cinfiguration code in inherited classes
            config = Config;

            if (device == null)
                device = new I2CDevice(config);
        }


        protected Int32 Read(Byte[] ReadBuffer)
        {
            Int32 retVal = -1;

            lock (device)
            {
                device.Config = config;

                I2CDevice.I2CTransaction[] transaction = new I2CDevice.I2CTransaction[] 
                {
                    I2CDevice.CreateReadTransaction(ReadBuffer)
                };

                retVal = device.Execute(transaction, 100);
            }

            return retVal;
        }

        protected Int32 ReadRegister(Byte Register, Byte[] ReadBuffer)
        {
            return WriteRead(new Byte[] { Register }, ReadBuffer);
        }


        protected Int32 Write(Byte[] WriteBuffer)
        {
            Int32 retVal = -1;

            lock (device)
            {
                device.Config = config;

                I2CDevice.I2CTransaction[] transaction = new I2CDevice.I2CTransaction[]
                {
                    I2CDevice.CreateWriteTransaction(WriteBuffer)
                };

                retVal = device.Execute(transaction, 100);
            }

            return retVal;
        }

        protected Int32 WriteRegister(Byte Register, Byte[] WriteBuffer)
        {
            Byte[] tmp = new Byte[WriteBuffer.Length + 1];

            tmp[0] = Register;
            WriteBuffer.CopyTo(tmp, 1);

            return Write(tmp);
        }


        protected Int32 WriteRead(Byte[] WriteBuffer, Byte[] ReadBuffer)
        {
            Int32 retVal = -1;

            lock (device)
            {
                device.Config = config;

                I2CDevice.I2CTransaction[] transaction = new I2CDevice.I2CTransaction[]
                {
                    I2CDevice.CreateWriteTransaction(WriteBuffer),
                    I2CDevice.CreateReadTransaction(ReadBuffer)
                };

                retVal = device.Execute(transaction, 100);
            }

            return retVal;
        }



        public I2CDevice Device
        {
            get
            {
                return device;
            }
        }

        public I2CDevice.Configuration Config
        {
            get
            {
                return config;
            }
        }


    }


}
